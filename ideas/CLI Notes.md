Usage:
```
note -n <name>
```
will open nvim where you can create a new note with title '<name>'
note -st
will open search with fzf or some other fuzzy finder, that will search in titles
note -sc
will search in context of a notes

Main idea:
When you using terminal you usually forget some flags or how to write class in JS)
WIth note you can write down your custom help for you in the future.
Smth like tldr and etc, but you should write by yourself.

Solutions:
1. probably it can be integrated with notion and nvim plugin for it, so it will be much faster to us it, you don't need to change workspace everytime you want to create note
2. write your own solution, but it will probably take some time and i'm a little bit lazy