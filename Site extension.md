## What can be improved?
- Architecture:
  Now extension downloads full OneAgent with support for all runtimes and it weights 100m+
  It can be solved by specifying runtime variable when link for downloading generates

- less rely on microsoft:
  - Now OneAgent can be installed only throw site extension
    We can ship installer both as site extension and as script by packing script into nuget package or just using script
  - Site extension is based on a server, which leads to issue with port binding.
    Using only script will prevent from this

- no UI
  Big customers don't deploy manually site ext and care more about stability, so providing only download functionality will increase stability.

## In details
Language: Go


## Why site ext need improvment?
Supporting site extension takes alsmotly all my time and not only mine.
### Why it takes so much time?
Usually you need to dig into the code to understand what is happening. And code it pretty
Support of vm extension takes much less.

## Main issues:

### Port binding
##### Error message:
```
  Unable to start Kestrel. System.Net.Sockets.SocketException (10013): An attempt was made to access a socket in a way forbidden by its access permissions
```
##### Tickets:
  - [PCLOUDS-1739](https://dev-jira.dynatrace.org/browse/PCLOUDS-1739)
  - [PCLOUDS-1397](https://dev-jira.dynatrace.org/browse/PCLOUDS-1397)
  - [PCLOUDS-1673](https://dev-jira.dynatrace.org/browse/PCLOUDS-1673)
##### Issue status:
Problem not solved. I was trying load testing to exceed limit of tcp connections, but site extension still starts perfectly. Probably this issue happens when kudu tries to open excluded port(i was getting the same error, when i was trying it on windows)
##### Possible solutions
- specify port for site extension, to 

